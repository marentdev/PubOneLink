﻿/*
 * Author : Baptiste Coudray
 * Date : 28.1.2018
 * Project : OneLink
 * Project desc. : Send a message periodically on the shoutbox of RealityGaming.
 * Class :  Room.cs
 * Class desc. : Represent a room in the shoutbox
 */

namespace OneLink
{
    public struct Room
    {
        public int Id { get; }
        public string Title { get; }

        public Room(int id, string title)
        {
            this.Id = id;
            this.Title = title;
        }

        public override string ToString()
        {
            return this.Title;
        }
    }
}
