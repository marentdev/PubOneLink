﻿/*
 * Author : Baptiste Coudray
 * Date : 21.1.2018
 * Project : OneLink
 * Project desc. : Send a message periodically on the shoutbox of RealityGaming.
 * Class :  XfTokenNotFoundException.cs
 * Class desc. : Exception that can be throw when the program can't retrieve the XfToken
 */
using System;
using System.Runtime.Serialization;

namespace OneLink
{
    public class XfTokenNotFoundException : Exception
    {
        public XfTokenNotFoundException()
        {
        }

        public XfTokenNotFoundException(string message) : base(message)
        {
        }

        public XfTokenNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected XfTokenNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
