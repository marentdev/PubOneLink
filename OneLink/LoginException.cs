﻿/*
 * Author : Baptiste Coudray
 * Date : 21.1.2018
 * Project : OneLink
 * Project desc. : Send a message periodically on the shoutbox of RealityGaming.
 * Class :  LoginException.cs
 * Class desc. : Exception that can be throw when an error is occured during the connection
 */
using System;
using System.Runtime.Serialization;

namespace OneLink
{
    public class LoginException : Exception
    {
        public LoginException() : base()
        {
        }

        public LoginException(string message) : base(message)
        {
        }

        public LoginException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LoginException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
