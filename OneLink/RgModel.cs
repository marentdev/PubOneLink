﻿/*
 * Author : Baptiste Coudray, MarentDev
 * Date : 21.1.2018
 * Project : OneLink
 * Project desc. : Send a message periodically on the shoutbox of RealityGaming.
 * Class :  RgModel.cs
 * Class desc. : Main class, brain of the project
 */
using RestSharp;
using System.Net;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using System;
using System.Collections.Generic;

namespace OneLink
{
    public class RgModel
    {
        private const string DEFAULT_URL = "https://realitygaming.fr/";
        private const string DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.89 Safari/537.36";
        private const string DEFAULT_WATERMARK = "#PubOneLink";
        public static readonly int DEFAULT_MAX_LENGTH_MESSAGE = 1000 - DEFAULT_WATERMARK.Length;
        private const string DEFAULT_REGISTERY_SUB_KEY = "Software\\PubOneLink";
        private const string DEFAULT_XF_TOKEN__HTML_INPUT = "<input type=\"hidden\" name=\"_xfToken\" value=\"(.*?)\"";
        private const string DEFAULT_LOGIN_HTML_ERROR = "<div class=\"blockMessage blockMessage--error blockMessage--iconic\">[\r\n]*(.*)[\r\n]*<";
        private const string DEFAULT_PSEUDO_KEY = "PseudoRG";
        private const string DEFAULT_MESSAGE_KEY = "MessageShoutbox";
        private const string DEFAULT_ROOM_ID_HTML_INPUT = "<a role=\"button\" data-target=\"room\" data-room-id=\"(\\d+)\" data-title=\"(.*?)\"";

        private RestClient Client { get; set; }
        private CookieContainer Cookie { get; set; }
        public string Pseudo
        {
            get
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(RgModel.DEFAULT_REGISTERY_SUB_KEY);
                string pseudo = (string)key.GetValue(RgModel.DEFAULT_PSEUDO_KEY) ?? string.Empty;
                key.Close();
                return pseudo;
            }
            private set
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(RgModel.DEFAULT_REGISTERY_SUB_KEY);
                key.SetValue(RgModel.DEFAULT_PSEUDO_KEY, value);
                key.Close();
            }
        }
        private string Password { get; set; }
        public string Message
        {
            get
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(RgModel.DEFAULT_REGISTERY_SUB_KEY);
                string message = (string)key.GetValue(RgModel.DEFAULT_MESSAGE_KEY) ?? string.Empty;
                key.Close();
                return message;
            }
            private set
            {
                string message = value.Trim();
                if (String.IsNullOrEmpty(message) || String.IsNullOrWhiteSpace(message))
                {
                    throw new ArgumentNullException("Message", "Message cannot be null or empty.");
                }
                if (message.Length > RgModel.DEFAULT_MAX_LENGTH_MESSAGE)
                {
                    message = message.Substring(0, RgModel.DEFAULT_MAX_LENGTH_MESSAGE);
                }
                RegistryKey key = Registry.CurrentUser.CreateSubKey(RgModel.DEFAULT_REGISTERY_SUB_KEY);
                key.SetValue(RgModel.DEFAULT_MESSAGE_KEY, message.Replace($" {RgModel.DEFAULT_WATERMARK}", ""));
                key.Close();

            }
        }
        private string XfToken { get => this.GetXfToken(); }
        public bool IsConnected { get; private set; }

        public RgModel(string pseudo, string password, string message)
        {
            this.Initialize();
            this.Set(pseudo, password, message);
        }

        public RgModel()
        {
            this.Initialize();
        }

        private void Initialize()
        {
            this.Client = new RestClient(RgModel.DEFAULT_URL)
            {
                UserAgent = RgModel.DEFAULT_USER_AGENT
            };
            this.Cookie = new CookieContainer();
            this.Client.CookieContainer = this.Cookie;
        }

        private string GetXfToken()
        {
            string xfToken = string.Empty;
            Tuple<RestResponse, HttpStatusCode> homePage = this.GoTo("index.php");
            if (homePage.Item2 == HttpStatusCode.OK)
            {
                Match r = Regex.Match(homePage.Item1.Content, RgModel.DEFAULT_XF_TOKEN__HTML_INPUT);

                if (r.Success)
                {
                    xfToken = r.Groups[1].Captures[0].Value;
                }
                else
                {
                    throw new XfTokenNotFoundException("Unable to find the XfToken.");
                }
            }
            return xfToken;
        }

        private Tuple<RestResponse, HttpStatusCode> GoTo(string page)
        {
            RestRequest request = new RestRequest(page, Method.GET);
            RestResponse response = (RestResponse)this.Client.Execute(request);
            return new Tuple<RestResponse, HttpStatusCode>(response, response.StatusCode);
        }

        public void Connect()
        {
            if (this.GoTo("login/login").Item2 == HttpStatusCode.OK)
            {
                RestRequest request = new RestRequest("login/login", Method.POST);
                request.AddParameter("_xfToken", this.XfToken);
                request.AddParameter("login", this.Pseudo);
                request.AddParameter("password", this.Password);
                request.AddParameter("remember", 1);
                RestResponse response = (RestResponse)this.Client.Execute(request);

                if (response.Content.Contains("<div class=\"p-navgroup p-account p-navgroup--member\">") && response.Content.Contains($"<span class=\"p-navgroup-linkText\">{this.Pseudo}</span>"))
                {
                    this.IsConnected = true;
                }
                else if (response.ResponseUri.LocalPath == "/login/login")
                {
                    this.HandleConnectionError(response);
                }
                else if (response.Content.Contains("La vérification en deux étapes est requise"))
                {
                    this.HandleTwoFactorSecurity(response);
                }
                else
                {
                    throw new LoginException("La connexion à RealityGaming a échoué.");
                }
            }
            else
            {
                throw new LoginException("Impossible d'obtenir la page d'accueil.");
            }
        }

        private void HandleConnectionError(RestResponse response)
        {
            Match r = Regex.Match(response.Content, RgModel.DEFAULT_LOGIN_HTML_ERROR);
            if (r.Success)
            {
                throw new LoginException(r.Groups[1].Captures[0].Value);
            }
            else
            {
                throw new LoginException("Une erreur est survenue durant la connexion à RealityGaming.");
            }
        }

        private void HandleTwoFactorSecurity(RestResponse response)
        {
            while (response.ResponseUri.LocalPath != "/")
            {
                if (response.ResponseUri.LocalPath == "/login/two-step")
                {
                    RgTwoFactorView frmTwoFactor = new RgTwoFactorView();
                    if (frmTwoFactor.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        RestRequest request = new RestRequest("login/two-step", Method.POST);
                        request.AddParameter("confirm", 1);
                        request.AddParameter("_xfToken", this.XfToken);
                        request.AddParameter("code", frmTwoFactor.CodeTwoFactor);
                        request.AddParameter("provider", "totp");
                        request.AddParameter("redirect", "");
                        request.AddParameter("remember", 1);
                        request.AddParameter("save", "Confirmer");
                        request.AddParameter("trust", 1);
                        response = (RestResponse)this.Client.Execute(request);
                    }
                }
                else
                {
                    throw new LoginException("Une erreur s'est produite durant la vérification en deux étapes.");
                }
            }
        }

        public bool SendMessage(Room selectedRoom)
        {
            RestRequest request = new RestRequest("chat/submit", Method.POST);
            request.AddParameter("channel", "room");
            request.AddParameter("room_id", selectedRoom.Id);
            request.AddParameter("conv_id", 6);
            request.AddParameter("message", $"{this.Message} {RgModel.DEFAULT_WATERMARK}");
            request.AddParameter("_xfResponseType", "json");
            request.AddParameter("_xfToken", this.XfToken);
            RestResponse response = (RestResponse)this.Client.Execute(request);
            return (response.ResponseStatus == ResponseStatus.Completed);
        }

        public List<Room> GetRooms()
        {
            List<Room> rooms = new List<Room>();
            var response = this.GoTo("index.php");
            if (response.Item2 == HttpStatusCode.OK)
            {
                MatchCollection r = Regex.Matches(response.Item1.Content, RgModel.DEFAULT_ROOM_ID_HTML_INPUT);
                if (r.Count > 0)
                {
                    foreach (Match item in r)
                    {
                        rooms.Add(new Room(Convert.ToInt32(item.Groups[1].Value), item.Groups[2].Value));
                    }
                }
            }
            return rooms;
        }

        public void Set(string pseudo, string password, string message)
        {
            this.Pseudo = pseudo;
            this.Password = password;
            this.Message = message;
        }
    }
}

