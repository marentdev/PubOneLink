﻿/*
 * Author : Baptiste Coudray
 * Date : 21.1.2018
 * Project : OneLink
 * Project desc. : Send a message periodically on the shoutbox of RealityGaming.
 * Class :  RgMainView.cs
 * Class desc. :
 */
using System;
using System.Windows.Forms;

namespace OneLink
{
    public partial class RgMainView : Form
    {
        private const int DEFAULT_BALLOON_TIP_TIMEOUT = 10;
        private const int DEFAULT_TIMER_INTERVAL = 3600000;

        private RgModel RgModel { get; set; }
        private System.Timers.Timer Timer { get; set; }

        public RgMainView()
        {
            this.InitializeComponent();
            this.tbxMessage.MaxLength = RgModel.DEFAULT_MAX_LENGTH_MESSAGE;
            this.RgModel = new RgModel();
            this.Timer = new System.Timers.Timer(RgMainView.DEFAULT_TIMER_INTERVAL);
            this.Timer.Elapsed += Timer_Elapsed;
            this.tbxPseudo.Text = this.RgModel.Pseudo;
            this.tbxMessage.Text = this.RgModel.Message;
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.RgModel.SendMessage((Room)this.cmbRoom.SelectedItem);
            this.lblNextMessage.Text = DateTime.Now.AddMilliseconds(Convert.ToDouble(this.Timer.Interval)).ToShortTimeString();
            this.ntfyOneLink.BalloonTipText = $"Message Envoyé !{Environment.NewLine}Prochain Message : {lblNextMessage.Text}";
            this.ntfyOneLink.Text = $"Prochain Message : {lblNextMessage.Text}";
            this.ntfyOneLink.ShowBalloonTip(RgMainView.DEFAULT_BALLOON_TIP_TIMEOUT);
        }

        private void TbxPseudo_TextChanged(object sender, EventArgs e)
        {
            this.pbRefresh.Enabled = (this.tbxPseudo.Text.Length > 0 && this.tbxPassword.Text.Length > 0);
            this.btnStart.Enabled = (this.tbxPseudo.Text.Length > 0 && this.tbxPassword.Text.Length > 0 && this.tbxMessage.Text.Length > 0 && this.cmbRoom.SelectedIndex > -1);
        }

        private void RgMainView_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                this.ntfyOneLink.Visible = true;
            }
        }

        private void NtfyOneLink_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            this.BringToFront();
            this.ntfyOneLink.Visible = false;
        }

        private void RgMainView_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Timer.Dispose();
        }

        private void TbInterval_ValueChanged(object sender, EventArgs e)
        {
            if (this.tbInterval.Value == 1)
                this.Timer.Interval = 3600000;
            else if (this.tbInterval.Value == 2)
                this.Timer.Interval = 3600000 + 1800000;
            else if (this.tbInterval.Value == 3)
                this.Timer.Interval = 3600000 * 2;
            else if (this.tbInterval.Value == 4)
                this.Timer.Interval = 3600000 * 2 + 1800000;
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            if (!this.Timer.Enabled)
            {
                this.Timer.Start();
                this.Timer_Elapsed(sender, null);
                this.btnStart.Text = "Arrêter";
                this.tbxPseudo.Enabled = false;
                this.tbxPassword.Enabled = false;
            }
            else
            {
                this.lblNextMessage.Text = "xx:xx";
                this.ntfyOneLink.Text = "Pub OneLink";
                this.btnStart.Text = "Démarrer";
                this.tbxPseudo.Enabled = true;
                this.tbxPassword.Enabled = true;
                this.Timer.Stop();
            }
        }

        private void PbRefresh_Click(object sender, EventArgs e)
        {
            this.RgModel.Set(tbxPseudo.Text, tbxPassword.Text, tbxMessage.Text);
            try
            {
                this.RgModel.Connect();
                this.cmbRoom.DataSource = this.RgModel.GetRooms();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
