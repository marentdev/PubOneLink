﻿/*
 * Author : Baptiste Coudray
 * Date : 21.1.2018
 * Project : OneLink
 * Project desc. : Send a message periodically on the shoutbox of RealityGaming.
 * Class :  RgTwoFactorView.cs
 * Class desc. :
 */
using System;
using System.Windows.Forms;

namespace OneLink
{
    public partial class RgTwoFactorView : Form
    {
        public string CodeTwoFactor { get; private set; }

        public RgTwoFactorView()
        {
            InitializeComponent();
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            this.CodeTwoFactor = this.tbxTwoFactorCode.Text;
        }

        private void TbxTwoFactorCode_TextChanged(object sender, EventArgs e)
        {
            this.btnOK.Enabled = this.tbxTwoFactorCode.Text.Length > 0;
        }

        private void TbxTwoFactorCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar));
        }
    }
}
