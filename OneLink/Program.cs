﻿/*
 * Author : Baptiste Coudray
 * Date : 21.1.2018
 * Project : OneLink
 * Project desc. : Send a message periodically on the shoutbox of RealityGaming.
 * Class :  Program.cs
 * Class desc. : Entry point
 */
using System;
using System.Windows.Forms;

namespace OneLink
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Length > 1)
            {
                MessageBox.Show("L'application est déjà en cours d'éxécution.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            Application.Run(new RgMainView());
        }
    }
}
